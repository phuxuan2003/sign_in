﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS_customer;
using DTO_customer;

namespace GUI_customer
{
    public partial class Sign_in : Form
    {
        public Sign_in()
        {
            InitializeComponent();
        }
        Bus_customer bus = new Bus_customer();
        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "hide")
            {
                button1.Text = "show";
            }
            else
            {
                button1.Text = "hide";
            }
            textBox2.UseSystemPasswordChar = !textBox2.UseSystemPasswordChar;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                Dto_customer data = new Dto_customer();
                data.Code = textBox1.Text;
                data.Pass = textBox2.Text;
                if (bus.Check_account(data))
                {
                   DialogResult res= MessageBox.Show("Sign in successfully!\nDo you want to update your information?", "", MessageBoxButtons.YesNo);
                    if (res == DialogResult.Yes)
                    {
                        Update_information udi = new Update_information(this.textBox1.Text);
                        udi.Show();
                    }
                    else
                    {
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Wrong account!");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Forget_password fg = new Forget_password();
            fg.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Sign_up sg = new Sign_up();
            sg.Show();
        }
    }
}
