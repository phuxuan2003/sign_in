﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS_customer;
using DTO_customer;

namespace GUI_customer
{
    public partial class Forget_password : Form
    {
        public Forget_password()
        {
            InitializeComponent();
        }
        Bus_customer bus = new Bus_customer();
        private void button1_Click(object sender, EventArgs e)
        {
            Dto_customer data = new Dto_customer();
            if (button1.Text=="Check")
            {
                data.Code = textBox1.Text;
                data.Per_id = textBox2.Text;
                data.Phone = textBox3.Text;
                if (bus.Check_per_id_phone(data))
                {
                    button1.Text = "Reset";
                    textBox1.Visible = false;
                    textBox2.Visible = false;
                    textBox3.Visible = false;
                    textBox4.Visible = true;
                    textBox5.Visible = true;
                }
                else
                {
                    MessageBox.Show("Wrong information!");
                }
                
            }
            else
            {
                if (textBox4.Text == textBox5.Text)
                {
                    data.Code = textBox1.Text;
                    data.Pass = textBox4.Text;
                    bus.Update_pass(data);
                    MessageBox.Show("Reset password successfully!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Check your password again!");
                }
            }
        }
    }
}
