﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DTO_customer;

namespace DAL_customer
{
    public class DB_connect
    {
       protected SqlConnection con = new SqlConnection(@"Data Source=MAYTINH-7NFC43P\SQLEXPRESS;Initial Catalog=my_database;Integrated Security=True");
    }
    public class Dal_customer : DB_connect
    {
        // kiem tra tai khoan va mat khau de dang nhap
        public bool check_accout(Dto_customer data)
        {
            con.Open();
            SqlCommand cm = new SqlCommand("Select password from customer where code =" + "'" + data.Code + "'", con);
            SqlDataReader sdr = cm.ExecuteReader();
            if (sdr.Read())
            {
                if (sdr["password"].ToString() == data.Pass)
                {
                    con.Close();
                    return true;
                }
            }
            con.Close();
            return false;
        }
        // dang nhap thanh cong va update lai thong tin cua user
        public void update_information(Dto_customer data, string old_code)
        {
            con.Open();
            SqlCommand cm = new SqlCommand("update customer set name ='" + data.Name 
                + "', code='" + data.Code + "',password='" + data.Pass +
                "', country_id='" + data.Cnt + "', city_id='" + data.Ct + 
                "', district_id='" + data.Dt + "', street='" + data.Street + 
                "', person_id='" + data.Per_id + "', phone_number='" + data.Phone + 
                "' where code='" + old_code + "'", con);
            cm.ExecuteNonQuery();
            con.Close();
        }
        // user quen mat khau, kiem tra tai khoan, person_id va phone_number de xac minh thong tin
        public bool check_code_pid_phone(Dto_customer data)
        {
            con.Open();
            SqlCommand cm = new SqlCommand("Select person_id, phone_number from customer where code =" + "'" + data.Code + "'", con);
            SqlDataReader sdr = cm.ExecuteReader();
            if (sdr.Read())
            {
                if (sdr["person_id"].ToString() == data.Per_id && sdr["phone_number"].ToString() == data.Phone)
                {
                    con.Close();
                    return true;
                }
            }
            con.Close();
            return false;
        }
        // xac minh thong tin chinh xac, cap lai mat khau cho user
        public void update_password(Dto_customer data)
        {
            con.Open();
            SqlCommand cm = new SqlCommand("update customer SET password ='" + data.Pass + "' WHERE code ='" + data.Code + "'", con);
            cm.ExecuteNonQuery();
            con.Close();
        }
        //dang ky tai khoan moi
        public void sign_up(Dto_customer data)
        {
            con.Open();
            string ins = "INSERT dbo.customer (name, code, street, district_id, city_id, country_id, password, person_id, phone_number) VALUES ('" + data.Name + "', '" + data.Code + "','" + data.Street + "'," + data.Dt + "," + data.Ct + "," + data.Cnt + ",'" + data.Pass + "','" + data.Per_id + "','" + data.Phone + "')";
            SqlCommand cm = new SqlCommand(ins, con);
            cm.ExecuteNonQuery();
            con.Close();
        }

        //lay ra data cua tat ca customer 
        public void get_info(Dto_customer data, string code)
        {
            con.Open();
            SqlCommand cm = new SqlCommand("Select * from customer where code ='" + code + "'", con);
            SqlDataReader sdr = cm.ExecuteReader();
            sdr.Read();
            data.Name = sdr["name"].ToString();
            data.Code = sdr["code"].ToString();
            data.Pass = sdr["password"].ToString();
            data.Cnt = int.Parse(sdr["country_id"].ToString());
            data.Dt = int.Parse(sdr["district_id"].ToString());
            data.Ct = int.Parse(sdr["city_id"].ToString());
            data.Street = sdr["street"].ToString();
            data.Per_id = sdr["person_id"].ToString();
            data.Phone = sdr["phone_number"].ToString();
            con.Close();
        }
    }
}
