﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS_customer;
using DTO_customer;

namespace GUI_customer
{
    public partial class Update_information : Form
    {
        string old_code;
        public string Old_code
        {
            get { return old_code; }
            set { old_code = value; }
        }
            
        public Update_information(string old_code)
        {
            Old_code = old_code;
            InitializeComponent();
            get_info();
        }
 
        Bus_customer bus = new Bus_customer();

        public void get_info()
        {
            Dto_customer data = new Dto_customer();
            bus.Get_info(data, old_code);
            textBox1.Text = data.Name;
            textBox2.Text = data.Code;
            textBox3.Text = data.Street;
            textBox4.Text = data.Dt.ToString();
            textBox5.Text = data.Ct.ToString();
            textBox6.Text = data.Cnt.ToString();
            textBox7.Text = data.Pass;
            textBox8.Text = data.Per_id;
            textBox9.Text = data.Phone;
        }
        private void button1_Click(object sender, EventArgs e)
        {
           try
            {
                Dto_customer data = new Dto_customer();
                data.Name = textBox1.Text;
                data.Code = textBox2.Text;
                data.Street = textBox3.Text;
                data.Dt = int.Parse(textBox4.Text);
                data.Ct = int.Parse(textBox5.Text);
                data.Cnt = int.Parse(textBox6.Text);
                data.Pass = textBox7.Text;
                data.Per_id = textBox8.Text;
                data.Phone = textBox9.Text;
                bus.Update_Info(data,Old_code);
                MessageBox.Show("Update successfully!");
                this.Close();
            }
            catch
            {
                MessageBox.Show("Your information is not valid!\nPlease check and update again!");
            }
        }
    }
}
