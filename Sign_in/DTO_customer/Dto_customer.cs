﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO_customer
{
    public class Dto_customer
    {
        private string name;
        private string code;
        private string street;
        private int dt;
        private int ct;
        private int cnt;
        private string pass;
        private string per_id;
        private string phone;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        public string Street
        {
            get { return street; }
            set { street = value; }
        }
        public int Dt
        {
            get { return dt; }
            set { dt = value; }
        }
        public int Ct
        {
            get { return ct; }
            set { ct = value; }
        }
        public int Cnt
        {
            get { return cnt; }
            set { cnt = value; }
        }
        public string Pass
        {
            get { return pass; }
            set { pass = value; }
        }
        public string Per_id
        {
            get { return per_id; }
            set { per_id = value; }
        }
        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        //public Dto_customer(string name, string code, string street, int dt, int ct, int cnt, string pass, string per_id, string phone)
        //{
        //    Name = name;
        //    Code = code;
        //    Street = street;
        //    Dt = dt;
        //    Ct = ct;
        //    Cnt = cnt;
        //    Pass = pass;
        //    Per_id = per_id;
        //    Phone = phone;
        //}
    }
}
