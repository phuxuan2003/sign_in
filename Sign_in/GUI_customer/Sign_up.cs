﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS_customer;
using DTO_customer;

namespace GUI_customer
{
    public partial class Sign_up : Form
    {
        public Sign_up()
        {
            InitializeComponent();
        }
        Bus_customer bus = new Bus_customer();
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Dto_customer data = new Dto_customer();
                data.Name = textBox1.Text;
                data.Code = textBox2.Text;
                data.Street = textBox3.Text;
                data.Dt = int.Parse(textBox4.Text);
                data.Ct = int.Parse(textBox5.Text);
                data.Cnt = int.Parse(textBox6.Text);
                data.Pass = textBox7.Text;
                data.Per_id = textBox8.Text;
                data.Phone = textBox9.Text;
                bus.Sign_up(data);
                MessageBox.Show("Sign up successfully!");
                this.Close();
            }
            catch
            {
                MessageBox.Show("Your information is not valid!\nPlease check and sign up again!");
            }
        }
    }
}
