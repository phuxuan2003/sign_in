﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO_customer;
using DAL_customer;

namespace BUS_customer
{
    public class Bus_customer
    {
        Dal_customer dal = new Dal_customer();
        public bool Check_account(Dto_customer data)
        {
            return dal.check_accout(data);
        }
        public void Update_Info(Dto_customer data, string old_code)
        {
            dal.update_information(data, old_code);
        }
        public bool Check_per_id_phone(Dto_customer data)
        {
            return dal.check_code_pid_phone(data);
        }
        public void Update_pass(Dto_customer data)
        {
            dal.update_password(data);
        }
        public void Sign_up(Dto_customer data)
        {
            dal.sign_up(data);
        }

        public void Get_info(Dto_customer data, string code)
        {
            dal.get_info(data, code);
        }
    }
}
